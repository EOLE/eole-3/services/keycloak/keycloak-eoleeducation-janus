package org.eole.keycloak.janus.mappers;

import org.eole.keycloak.janus.provider.JanusIdentityProviderFactory;
import org.keycloak.broker.oidc.mappers.UsernameTemplateMapper;

public class JanusUsernameTemplateMapper extends UsernameTemplateMapper {

    private static final String MAPPER_NAME = "janus-username-template-mapper";

    @Override
    public String[] getCompatibleProviders() {
        return JanusIdentityProviderFactory.COMPATIBLE_PROVIDER;
    }

    @Override
    public String getId() {
        return MAPPER_NAME;
    }

}
