package org.eole.keycloak.janus.mappers;

import org.eole.keycloak.janus.provider.JanusIdentityProviderFactory;
import org.keycloak.broker.oidc.mappers.UserAttributeMapper;

public class JanusUserAttributeMapper extends UserAttributeMapper {

    private static final String MAPPER_NAME = "janus-user-attribute-mapper";

    @Override
    public String[] getCompatibleProviders() {
        return JanusIdentityProviderFactory.COMPATIBLE_PROVIDER;
    }

    @Override
    public String getId() {
        return MAPPER_NAME;
    }

}
