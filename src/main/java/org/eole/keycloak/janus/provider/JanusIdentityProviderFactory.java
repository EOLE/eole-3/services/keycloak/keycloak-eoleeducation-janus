package org.eole.keycloak.janus.provider;

import org.keycloak.broker.provider.AbstractIdentityProviderFactory;
import org.keycloak.broker.social.SocialIdentityProviderFactory;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;

public class JanusIdentityProviderFactory extends AbstractIdentityProviderFactory<JanusIdentityProvider>
        implements SocialIdentityProviderFactory<JanusIdentityProvider> {

    public static final String JANUS_PROVIDER_ID = "Janus-particulier";
    public static final String JANUS_PROVIDER_NAME = "Janus Eole Education";

    public static final String[] COMPATIBLE_PROVIDER = new String[]{ JANUS_PROVIDER_ID };

    @Override
    public String getName() {
        return JANUS_PROVIDER_NAME;
    }

    @Override
    public String getId() {
        return JANUS_PROVIDER_ID;
    }

    @Override
    public JanusIdentityProvider create(KeycloakSession session, IdentityProviderModel model) {
        return new JanusIdentityProvider(
            session,
            new JanusIdentityProviderConfig(model)
        );
    }

    @Override
    public JanusIdentityProviderConfig createConfig() {
        return new JanusIdentityProviderConfig();
    }

}
